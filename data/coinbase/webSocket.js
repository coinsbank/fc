/*global WebSocket */
import d3 from 'd3';

// https://docs.exchange.coinbase.com/#websocket-feed

export default function() {

    var product = 'BTC-USD';

    var dispatch = d3.dispatch('open', 'close', 'error', 'message');
    var messageType = 'match';
    var socket;







    var webSocket = function(url, subscribe) {
        //url = url || 'wss://ws-feed.exchange.coinbase.com';
        var socketUrl='http://'+config.appData.app.host+':'+config.appData.app.socketport;
        url = url || 'wss://ws-feed.exchange.coinbase.com';

        var room = config.defaultparams.market+config.defaultparams.pair;

        socket = io.connect(socketUrl, {
            'sync disconnect on unload': true ,
            'reconnect': true,
            'reconnection delay': 500,
            'max reconnection attempts': 100});


        //socket = new WebSocket(url);
        socket.on('connect', function (event) {
            console.log('connected')
            socket.emit('room', room);
            dispatch.open(event);
        });

        socket.on('disconnect', function(){
            // reconnect
            dispatch.close(event);
            console.log('seems like server goes offline, trying to reconnect in 5 s');

        });


        //tell socket.io to never give up :)
        socket.on('error', function(){
            //socket.socket.reconnect();
            console.log('ng: socket error');
            dispatch.error(event);
        });




        socket.on('match', function(msg){
            //socket.broadcast.to(id).emit('my message', msg);
            //console.log(msg.time)
            dispatch.message(msg);
        });

    };



    d3.rebind(webSocket, dispatch, 'on');

    webSocket.close = function() {
        if (socket) {
            socket.close();
        }
    };

    webSocket.messageType = function(x) {
        if (!arguments.length) {
            return messageType;
        }
        messageType = x;
        return webSocket;
    };

    webSocket.product = function(x) {
        if (!arguments.length) {
            return product;
        }
        product = x;
        return webSocket;
    };

    return webSocket;
}

