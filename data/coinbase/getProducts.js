import d3 from 'd3';
import model from '../../model/model';

export default function(callback) {
    d3.json('https://api.exchange.coinbase.com/products', function(error, response) {
        if (error) {
            callback(error);
            return;
        }
        callback(error, response);
    });
}


    //[{"id":"BTC-USD","base_currency":"BTC","quote_currency":"USD","base_min_size":"0.01","base_max_size":"10000","quote_increment":"0.01","display_name":"BTC/USD"},{"id":"BTC-GBP","base_currency":"BTC","quote_currency":"GBP","base_min_size":"0.01","base_max_size":"10000","quote_increment":"0.01","display_name":"BTC/GBP"},{"id":"BTC-EUR","base_currency":"BTC","quote_currency":"EUR","base_min_size":"0.01","base_max_size":"10000","quote_increment":"0.01","display_name":"BTC/EUR"},{"id":"BTC-CAD","base_currency":"BTC","quote_currency":"CAD","base_min_size":"0.01","base_max_size":"10000","quote_increment":"0.01","display_name":"BTC/CAD"}]