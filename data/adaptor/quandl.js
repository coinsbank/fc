import d3 from 'd3';
import fc from 'd3fc';
import chartchain from '../feed/chartchain';


export default function() {

    //var historicFeed = fc.data.feed.quandl(),
    var historicFeed = chartchain(),
        candles;

    // More options are allowed through the API; for now, only support daily and weekly
    //var allowedPeriods = d3.map();
    //
    //allowedPeriods.set(60, '1m');
    //allowedPeriods.set(5*60, '5m');
    //allowedPeriods.set(10*60, '10m');
    //allowedPeriods.set(30*60, '30m');
    //allowedPeriods.set(3600, '1h');
    //allowedPeriods.set(6*3600, '6h');
    //allowedPeriods.set(60 * 60 * 24, 'daily');

    function quandlAdaptor(cb) {

        //var startDate = d3.time.second.offset(historicFeed.end(), -candles * granularity);
        var startDate = d3.time.second.offset(historicFeed.end(), -candles * historicFeed.granularity());

        //console.log(startDate) ;
        //console.log('<-quandlAdaptor startDate')


        //historicFeed.start(historicFeed.end());
        historicFeed.start(startDate);
        historicFeed(cb);

        //.cb('null',[]);
        //historicFeed.start(startDate).apiKey('yJZg7GxzrZEsqtf_W-r5')
        //    .collapse(allowedPeriods.get(granularity));
        //historicFeed(cb);

    }

    quandlAdaptor.candles = function(x) {
        if (!arguments.length) {
            return candles;
        }
        candles = x;
        return quandlAdaptor;
    };

    //quandlAdaptor.granularity = function(x) {
    //    if (!arguments.length) {
    //        return granularity;
    //    }
    //    if (!allowedPeriods.has(x)) {
    //        throw new Error('Granularity of ' + x + ' is not supported q.');
    //    }
    //    granularity = x;
    //    return quandlAdaptor;
    //};

    d3.rebind(quandlAdaptor, historicFeed, 'end', 'granularity', 'product');

    //fc.util.rebind(quandlAdaptor, historicFeed, {
    //    end: 'end',
    //    product: 'dataset'
    //});

    return quandlAdaptor;
}
