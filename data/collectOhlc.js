import d3 from 'd3';

export default function() {

    var date = function(d) { return d.date; };
    var volume = function(d) { return Number(d.volume); };
    var price = function(d) { return Number(d.price); };
    var granularity = 60;

    function getBucketStart(tradeDate) {

        var granularityInMs = granularity * 1000;
        //console.log('getBucketStart');
        //console.log(new Date(Math.floor(tradeDate.getTime() / granularityInMs) * granularityInMs));

        return new Date(Math.floor(tradeDate.getTime() / granularityInMs) * granularityInMs);
    }



    var collectOhlc = function(data, trade) {

        //console.log('collectOhlc');
        //console.log(data, trade);

        var bucketStart = getBucketStart(date(trade));

        //console.log(bucketStart);
        //console.log('bucketStart');


        var tradePrice = price(trade);
        var tradeVolume = volume(trade);
        var bisectDate = d3.bisector(function(d) { return d.date; }).left;
        var existing = data.filter(function(d) {
            return d.date.getTime() === bucketStart.getTime();
        })[0];

        if (existing) {
            //console.log('existing');
            //check if prev close < curr open => curr open = prev close
            existing.high = Math.max(tradePrice, existing.high);
            existing.low = Math.min(tradePrice, existing.low);
            existing.close = tradePrice;
            existing.volume += tradeVolume;

        } else {
            console.log('adding not existing bar');
            var tradePriceOpen=tradePrice;
            if(data[data.length-1].close!=tradePriceOpen){
                tradePriceOpen=data[data.length-1].close;
                console.log('fixing close');
            }
            //find last bar witch value
            //find delta btw last
            //calc delta btw closing and opening


            console.log('last item date');
            console.log(data[data.length-1].date);

            var tmpDateLastInMs=data[data.length-1].date.getTime();
            var tmpDateNewInMs=bucketStart.getTime();
            var currentIntervalInMs=granularity * 1000;
            var missedIntervalsCount=(tmpDateNewInMs-tmpDateLastInMs)/currentIntervalInMs;



            console.log('new int', bisectDate(data, bucketStart))

            console.log('---');
            console.log(missedIntervalsCount);
            console.log('---');
            console.log(tmpDateLastInMs);
            console.log(tmpDateNewInMs);
            //console.log(tmpDateNewInMs-currentIntervalInMs);
            console.log('---');


            var newItemInsertPosition=bisectDate(data, bucketStart);
            console.log(newItemInsertPosition);
            console.log(data.length);
            console.log('last date pice' ,data[data.length-1].date);

            if(missedIntervalsCount>1){ //1 its ok
                //will add few date
                console.warn('+', missedIntervalsCount);
                console.warn('will fill n intervals', missedIntervalsCount);


                for(i=0;  i < missedIntervalsCount-1; i++ ){
                    newItemInsertPosition++;
                    var nDate=new Date(tmpDateLastInMs+(currentIntervalInMs*(i+1)));

                console.log('add new item in int', nDate);
                console.log('for date', nDate);
                    data.splice(newItemInsertPosition, 0, {
                        date: nDate,
                        open: tradePrice,
                        high: tradePrice,
                        low: tradePrice,
                        close: tradePrice,
                        volume: tradeVolume
                    });
                }
            }

            //add new array
            //data.splice(bisectDate(data, bucketStart), 0, {
            newItemInsertPosition++;

console.log('Original last item pos:', newItemInsertPosition);
console.log('Original last item date:', bucketStart);



            data.splice(newItemInsertPosition, 0, {
                date: bucketStart,
                open: tradePriceOpen,
                high: tradePrice,
                low: tradePrice,
                close: tradePrice,
                volume: tradeVolume
            });


            console.log('last date pice' ,data[data.length-1].date, data.length-1);
            console.log('last date pice' ,data[data.length-2].date, data.length-2);
            console.log('last date pice' ,data[data.length-3].date, data.length-3);
            console.log('last date pice' ,data[data.length-4].date, data.length-4);
        }
    };

    collectOhlc.granularity = function(x) {
        if (!arguments.length) {
            return granularity;
        }
        granularity = x;
        return collectOhlc;
    };

    collectOhlc.price = function(x) {
        if (!arguments.length) {
            return price;
        }
        price = x;
        return collectOhlc;
    };

    collectOhlc.volume = function(x) {
        if (!arguments.length) {
            return volume;
        }
        volume = x;
        return collectOhlc;
    };

    collectOhlc.date = function(x) {
        if (!arguments.length) {
            return date;
        }
        date = x;
        return collectOhlc;
    };

    return collectOhlc;
}
