import d3 from 'd3';

// https://docs.exchange.chartchain.com/#market-data
export default function() {

    var product = 'BTC-USD',
        start = null,
        end = null,
        granularity = null;

    var chartchain = function(cb) {
        var params = [];
        //if (start != null) {
        //    params.push('start=' + start.toISOString());
        //}
        //if (end != null) {
        //    params.push('end=' + end.toISOString());
        //}
        //if (granularity != null) {
        //    params.push('granularity=' + granularity);
        //}
        //var url = 'https://api.exchange.chartchain.com/products/' + product + '/candles?' + params.join('&');

        var dtime=granularity/60;
        var url = config.appData.app.url+'/api/'+config.defaultparams.market+"/"+config.defaultparams.pair+"/ohlc?interval="+dtime;

        d3.json(url, function(error, data) {
            if (error) {
                cb(error);
                return;
            }
            data = data.map(function(d) {
                return {
                    date: new Date(d.date),
                    open: +d.o,
                    high: +d.h,
                    low: +d.l,
                    close: +d.c,
                    volume: +d.v
                };
            });
            cb(error, data);

        });
    };

    chartchain.product = function(x) {
        if (!arguments.length) {
            return product;
        }
        product = x;
        return chartchain;
    };
    chartchain.start = function(x) {
        if (!arguments.length) {
            return start;
        }
        start = x;
        return chartchain;
    };
    chartchain.end = function(x) {
        if (!arguments.length) {
            //console.log(end);
            //console.log('end');
            return end;
        }
        end = x;
        return chartchain;
    };
    chartchain.granularity = function(x) {
        if (!arguments.length) {
            return granularity;
        }
        granularity = x;
        return chartchain;
    };

    return chartchain;
}
