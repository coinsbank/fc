import app from './app';

// Needs to be defined like this so that the grunt task can update it
var version = 'development';

export default {
    app: app
};
