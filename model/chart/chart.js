import legend from './legend';
import nav from './nav';
import navigationReset from './navigationReset';
import primary from './primary';
import orders from './orders';
import secondary from './secondary';
import xAxis from './xAxis';

export default {
    legend: legend,
    nav: nav,
    navigationReset: navigationReset,
    primary: primary,
    orders: orders,
    secondary: secondary,
    xAxis: xAxis
};
