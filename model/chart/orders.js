export default function(yExtent) {
    return {
        data: [],
        lim: 0,
        viewDomain: [],
        dataLim: [],
        trackingLatest: true,
        yExtent: yExtent
    };
}
