import menu from './menu/menu';
import chart from './chart/chart';
import data from './data/data';

export default {
    menu: menu,
    chart: chart,
    data: data
};
