import indicatorOptions from './indicatorOptions';
import secondaryChartOptions from './secondaryChartOptions';

export default function() {
    return {
        config: {
            title: 'Indicators',
            careted: false,
            listIcons: true,
            icon: false
        },
        indicatorOptions: indicatorOptions,
        secondaryChartOptions: secondaryChartOptions
    };
}
