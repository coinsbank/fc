import selectors from './selectors';
import head from './head';
import periodAdaptor from './periodAdaptor';
import productAdaptor from './productAdaptor';
import overlay from './overlay';

export default {
    selectors: selectors,
    head: head,
    periodAdaptor: periodAdaptor,
    productAdaptor: productAdaptor,
    overlay: overlay
};
