export default function(historicFeed, streamingFeed) {
    return {
        historic: historicFeed,
        streaming: streamingFeed
    };
}
