import d3 from 'd3';
import fc from 'd3fc';
import util from '../util/util';
import event from '../event';
import option from '../model/menu/option';
import candlestick from '../series/candlestick';
import zoomBehavior from '../behavior/zoom';

function calculateCloseAxisTagPath(width, height) {
    var h2 = height / 2;
    return [
        [0, 0],
        [h2, h2],
        [width, h2],
        [width, -h2],
        [h2, -h2],
        [0, 0]
    ];
}

function calculateCloseAxisTagLine(width, height) {
    var h2 = height / 2;
    return [
        { "x": 0,   "y": 0},  { "x": h2,  "y": h2},
        { "x": width,  "y": h2}, { "x": width,  "y": -h2},
        { "x": h2,  "y": -h2}, { "x": 0,   "y": 0}
    ];
}

function produceAnnotatedTickValues(scale, annotation) {
    var annotatedTickValues = scale.ticks.apply(scale, []);

    var extent = scale.domain();
    for (var i = 0; i < annotation.length; i++) {
        if (annotation[i] > extent[0] && annotation[i] < extent[1]) {
            annotatedTickValues.push(annotation[i]);
        }
    }
    return annotatedTickValues;
}

function findTotalYExtent(visibleData, currentSeries, currentIndicators) {
    var extentAccessor;
    switch (currentSeries.valueString) {
    case 'candlestick':
    case 'ohlc':
        extentAccessor = [currentSeries.option.yLowValue(), currentSeries.option.yHighValue()];
        break;
    case 'line':
    case 'point':
        extentAccessor = currentSeries.option.yValue();
        break;
    case 'area' :
        extentAccessor = currentSeries.option.y1Value();
        break;
    default:
        throw new Error('Main series given to chart does not have expected interface');
    }
    var extent = fc.util.extent()
      .fields(extentAccessor)(visibleData);

    if (currentIndicators.length) {
        var indicators = currentIndicators.map(function(indicator) { return indicator.valueString; });
        var movingAverageShown = (indicators.indexOf('movingAverage') !== -1);
        var bollingerBandsShown = (indicators.indexOf('bollinger') !== -1);
        if (bollingerBandsShown) {
            var bollingerBandsVisibleDataObject = visibleData.map(function(d) { return d.bollingerBands; });
            var bollingerBandsExtent = fc.util.extent()
              .fields(['lower', 'upper'])(bollingerBandsVisibleDataObject);
            extent[0] = d3.min([bollingerBandsExtent[0], extent[0]]);
            extent[1] = d3.max([bollingerBandsExtent[1], extent[1]]);
        }
        if (movingAverageShown) {
            var movingAverageExtent = fc.util.extent()
              .fields('movingAverage')(visibleData);
            extent[0] = d3.min([movingAverageExtent[0], extent[0]]);
            extent[1] = d3.max([movingAverageExtent[1], extent[1]]);
        }
        if (!(movingAverageShown || bollingerBandsShown)) {
            throw new Error('Unexpected indicator type');
        }
    }
    return extent;
}

export default function() {

    var yAxisWidth = 70;
    var dispatch = d3.dispatch(event.viewChange, event.crosshairChange);
    var currentSeries;
    var currentYValueAccessor = function(d) { return d.close; };
    var currentIndicators = [];
    var zoomWidth;


    // Create a fibonacciFan tool
    var fibonacciFan = fc.tool.fibonacciFan()
        //.xScale(xScale)
        //.yScale(yScale)
        .on('fansource', function(d) {
           // console.log('fansource ' + d[0].source.x + ',' + d[0].source.y + '\n' );
        })
        .on('fantarget', function(d) {
           // console.log( 'fantarget ' + d[0].target.x + ',' + d[0].target.y + '\n');
        })
        .on('fanclear', function() {
           // console.log( 'fanclear\n')
        }).decorate(function(selection) {
            selection.enter()
                .append('circle')
                .attr('r', 6)
                .style('stroke', 'black')
                .style('fill', 'none');
            //selection.select('circle')
                //.attr('cx', function(d) { return d.target ? xScale(d.target.x) : 0; })
                //.attr('cy', function(d) { return d.target ? xScale(d.target.y) : 0; })
                //.style('visibility', function(d) { return d.state !== 'DONE' ? 'visible' : 'hidden'; });
        });

    //.xScale()
    //.yScale(priceScale)
    //.snap(fc.util.seriesPointSnap(bar, data))
    // Create scale for x axis
    //var width = 600, height = 250;
    var dateScale = xScale;
    // Calculate the scale domain
    //var day = 8.64e7, // One day in milliseconds
    //    dateFrom = new Date(d3.min(data, function(d) { return d.date; }).getTime() - day),
    //    dateTo = new Date(d3.max(data, function(d) { return d.date; }).getTime() + day),
    //    priceFrom = d3.min(data, function(d) { return d.low; }),
    //    priceTo = d3.max(data, function(d) { return d.high; });

    fibonacciFan.id = util.uid();
    var fibonacciFanData = [];



    var measureTool = fc.tool.measure()
        //.xScale(xScale)
        //.yScale(yScale)
        .on('measuresource', function(d) {
            console.log('measuresource ' + d[0].source.x + ',' + d[0].source.y )
        })
        .on('measuretarget', function(d) {
            console.log('measuretarget ' + d[0].target.x + ',' + d[0].target.y )
        })
        .on('measureclear', function() {
           console.log('measureclear\n')
        })
        .decorate(function(selection) {
            selection.enter()
                .append('circle')
                .attr('r', 6)
                .style('stroke', 'black')
                .style('fill', 'none');
            selection.select('circle')
                //.attr('cx', function(d) { return d.target ? xScale(d.target.x) : 0; })
                //.attr('cy', function(d) { return d.target ? xScale(d.target.y) : 0; })
                //.style('visibility', function(d) { return d.state !== 'DONE' ? 'visible' : 'hidden'; });
        });


    measureTool.id = util.uid();
    var measureToolData = [];



    var crosshairData = [];
    var crosshair = fc.tool.crosshair()
      .xLabel('')
      .yLabel('<-')
      .on('trackingmove', function(updatedCrosshairData) {
          if (updatedCrosshairData.length > 0) {
              dispatch.crosshairChange(updatedCrosshairData[0].datum);
          } else {
              dispatch.crosshairChange(undefined);
          }
      })
      .on('trackingend', function() {
          dispatch.crosshairChange(undefined);
      });
    crosshair.id = util.uid();

    //var gridlines = fc.annotation.gridline()
    //  .yTicks(5)
    //  .xTicks(0);

    var closeLine = fc.annotation.line()
      .orient('horizontal')
      .value(currentYValueAccessor)
      .label('');
    closeLine.id = util.uid();

    var multi = fc.series.multi()
        .key(function(series) { return series.id; })
        .mapping(function(series) {
            switch (series) {
            case closeLine:
                return [this.data[this.data.length - 1]];
            case crosshair:
                return crosshairData;
            case fibonacciFan:
                return fibonacciFanData;
            case measureTool:
                return measureToolData;
            default:
                return this.data;
            }
        });

    var xScale = fc.scale.dateTime();
    var yScale = d3.scale.linear();

    var primaryChart = fc.chart.cartesian(xScale, yScale)
        .xTicks(0)
        .yTicks(10)
        .decorate(function(sel) {
            sel.select('.x-axis')
                .style("visibility","hidden")
        })
        .margin({
            top: 0,
            left: 0,
            bottom: 0,
            right: 0 //yAxisWidth
        });

    // Create and apply the Moving Average
    var movingAverage = fc.indicator.algorithm.movingAverage();
    var bollingerAlgorithm = fc.indicator.algorithm.bollingerBands();

    function updateMultiSeries() {
        var baseChart = [currentSeries.option, closeLine]; //gridlines,
        var indicators = currentIndicators.map(function(indicator) { return indicator.option; });
        return baseChart.concat(indicators);
    }

    function updateYValueAccessorUsed() {
        movingAverage.value(currentYValueAccessor);
        bollingerAlgorithm.value(currentYValueAccessor);
        closeLine.value(currentYValueAccessor);
        switch (currentSeries.valueString) {
        case 'line':
        case 'point':
        case 'area':
            currentSeries.option.yValue(currentYValueAccessor);
            break;
        default:
            break;
        }
    }

    // Call when what to display on the chart is modified (ie series, options)
    function selectorsChanged(model) {
        currentSeries = model.series;
        currentYValueAccessor = model.yValueAccessor.option;
        currentIndicators = model.indicators;
        updateYValueAccessorUsed();


        var multiArray=updateMultiSeries();
        //console.log('selectorsChanged tool is: '+model.currentTool)

        switch (model.currentTool) {
            case 'crosshair':
                multiArray=multiArray.concat(crosshair);
                break;

            case 'fan':
                multiArray=multiArray.concat(fibonacciFan);
                break;

            case 'line':
                multiArray=multiArray.concat(measureTool);
                break;

            //no tool found, select default
            default:
                multiArray=multiArray.concat(crosshair);
                throw new Error('Tool not found');
        }


        multi.series(multiArray);
        primaryChart.yTickFormat(model.product.priceFormat);
        model.selectorsChanged = false;
    }

    function bandCrosshair(data) {

        var width = currentSeries.option.width(data);

        crosshair.decorate(function(selection) {
            selection.classed('band hidden-xs hidden-sm', true);

            selection.selectAll('.point')
                .style('visibility','hidden')

            selection.selectAll('.vertical > line')
              .style('stroke-width', width);
        });
    }

    function lineCrosshair(selection) {
        selection.classed('band', false)
            .classed('hidden-xs hidden-sm', true)
            .selectAll('line')
            .style('stroke-width', null);

        selection.selectAll('.point')
            .style('visibility','hidden')

    }

    function updateCrosshairDecorate(data) {
        if (currentSeries.valueString === 'candlestick' || currentSeries.valueString === 'ohlc') {
            bandCrosshair(data);
        } else {
            crosshair.decorate(lineCrosshair);
            //fibonacciFan.decorate(fibonacciFan);
        }
    }

    function primary(selection) {
        var model = selection.datum();

        if (model.selectorsChanged) {
            selectorsChanged(model);
        }

        primaryChart.xDomain(model.viewDomain);

        crosshair.snap(fc.util.seriesPointSnapXOnly(currentSeries.option, model.data));
        fibonacciFan.snap(fc.util.seriesPointSnap(currentSeries.option, model.data));
        measureTool.snap(fc.util.seriesPointSnap(currentSeries.option, model.data));

        updateCrosshairDecorate(model.data);

        movingAverage(model.data);
        bollingerAlgorithm(model.data);

        // Scale y axis
        var visibleData = util.domain.filterDataInDateRange(primaryChart.xDomain(), model.data);

        var yExtent = findTotalYExtent(visibleData, currentSeries, currentIndicators);


        //console.log(model.extentByOrders);
        //console.log('model.extentByOrders prim');


        var tmpExtent=util.domain.padYDomain(yExtent, 0.35);//[yExtent[0],yExtent[1]];
        //


//        if(model.extentByOrders.length>0){
//        //    if (yExtent[0] > parseInt(model.extentByOrders[0]) || yExtent[1] < parseInt(model.extentByOrders[1] || (yExtent[0] > parseInt(model.extentByOrders[0]) && yExtent[1] < parseInt(model.extentByOrders[1])))) {
//        //        console.log('<<< by orders');
//
//                //console.log(model.extentByOrders);
//console.warn(tmpExtent[0], '>', parseInt(model.extentByOrders[0]))
//console.warn(tmpExtent[1], '<', parseInt(model.extentByOrders[1]))
//                if (tmpExtent[0] > parseInt(model.extentByOrders[0])){
//                    tmpExtent[0] = parseInt(model.extentByOrders[0])
//                }
//
//                if (tmpExtent[1] > parseInt(model.extentByOrders[1])){
//                    tmpExtent[1] = parseInt(model.extentByOrders[1])
//                }
//            //}
//        }


        var paddedYExtent = model.extentByOrders;

        //var paddedYExtent = util.domain.padYDomain(yExtent, 0.35);
        //model.extentByOrders=yExtent;
        // Add percentage padding either side of extreme high/lows

        //primaryChart

        //fibonacciFan.yDomain(paddedYExtent);
        //This is the accessor function we talked about above

        var lineFunction = d3.svg.line()
                                 .x(function(d) { return d.x; })
                                 .y(function(d) { return d.y; })
                                 .interpolate("linear");

        // Find current tick values and add close price to this list, then set it explicitly below
        var latestPrice = currentYValueAccessor(model.data[model.data.length - 1]);

        var tickValues = produceAnnotatedTickValues(yScale, [latestPrice]);
        primaryChart
            .yDomain(paddedYExtent)
            .yTickValues(tickValues)
            .yDecorate(function(s) {
                  s.selectAll('.tick')
                      .select('text')
                      .attr("visible", "hidden");
                      //.attr("transform", "translate(-45,0)");

                  s.selectAll('.tick')
                      .select('path')
                      .attr("transform", "translate(-10,0)");

                  //s.selectAll('.tick')
                  //  .filter(function(d) { return d === latestPrice; })
                  //  .classed('closeLine', true)
                  //  .select('path')
                  //  .attr('d', lineFunction(calculateCloseAxisTagLine(yAxisWidth, 20)))
                  //    .attr("transform", "translate(-55,0)");


            });



        primaryChart.plotArea(multi);
        selection.call(primaryChart);

        var zoom = zoomBehavior(zoomWidth)
                .scale(xScale)
                .trackingLatest(model.trackingLatest)
                .minimumPeriods(model.minimumPeriods)
                .on('zoom', function(domain) {
                        dispatch[event.viewChange](domain);
                    });

        selection.select('.plot-area')
          .call(zoom);
    }

    d3.rebind(primary, dispatch, 'on');

    // Call when the main layout is modified
    primary.dimensionChanged = function(container) {
        zoomWidth = parseInt(container.style('width'), 10) - yAxisWidth;

    };

    return primary;
}
