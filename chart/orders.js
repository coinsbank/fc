import d3 from 'd3';
import fc from 'd3fc';
import util from '../util/util';
import event from '../event';
import base from './secondary/base';
//import candlestick from '../series/candlestick';


export default function () {
    var dispatch = d3.dispatch(event.viewChange);

    var yAxisWidth = 70;
    var ddata = d3.range(20).map(function (d) {
        return {
            buy_rate: 0,
            buy_sum: 0,
            sell_rate: 0,
            sell_sum: 0,
            med_rate: 0,
            med_sum: 0
        }
    })

    function calculateCloseAxisTagLine(width, height) {
        var h2 = height / 2;
        return [
            { "x": 0,   "y": 0},  { "x": h2,  "y": h2},
            { "x": width,  "y": h2}, { "x": width,  "y": -h2},
            { "x": h2,  "y": -h2}, { "x": 0,   "y": 0}
        ];
    }

 var xScale = d3.scale.linear()
        //.domain([0, 3981524]) //ddata.length

    var yScale = d3.scale.linear();



    var bline = fc.series.line()
        .decorate(function(sel) {
            sel.enter().classed('bline', true).attr("stroke", "red");
        })
        .interpolate('step')
        .xValue(function (d) { return Number(d.sumValue); })
        .yValue(function (d) { return Number(d.rate);   })



    var sline = fc.series.line()
        .decorate(function(sel) {
            sel.enter().classed('sline', true);
        })
        .interpolate('step')
        .xValue(function (d) {return Number(d.sumValue); })
        .yValue(function (d) { return Number(d.rate); });


    var mline = fc.series.line()
        .decorate(function(sel) {
            sel.enter().classed('mline', true);
        })
        .interpolate('line')
        .xValue(function (d) {return d.sumValue; })
        .yValue(function (d) { return d.rate; });


    var multi = fc.series.multi()
        .series([bline, sline, mline])
        .mapping(function(series) {
            switch (series) {
                case bline:
                    return this.buy;
                case sline:
                    return this.sell;
                case mline:
                    return this.med;
            }
        });


    function produceAnnotatedTickValues(scale, annotation) {
        var annotatedTickValues = scale.ticks.apply(scale, []);

        var extent = scale.domain();

        for (var i = 0; i < annotation.length; i++) {
            if (annotation[i] > extent[0] && annotation[i] < extent[1]) {
                annotatedTickValues.push(annotation[i]);
            }
        }
        return annotatedTickValues;
    }


    //currentYValueAccessor = model.yValueAccessor.option;
    //var latestPrice = currentYValueAccessor(model.data[model.data.length - 1]);

    //var tickValues = produceAnnotatedTickValues(yScale);

    var chartorder = fc.chart.cartesian(xScale, yScale)
        .xTicks(0)
        .yTicks(5)
        .yOrient('left')
        .margin({
            top: 0,
            left: 2,
            bottom: 0,
            right: 0
        })
        //.yDomain(yScale)
        //.yDecorate(function(s){
        //    s.selectAll('.tick')
        //        .append("path")
        //        .attr("d", 'M0,0L-6,0');
        //})
        //.xDomain([0, 0])


    chartorder.plotArea(multi);

    //var latestPrice=[415.63];


    function orders(selection) {
        var model = selection.datum();

        //selection.each(function (model) {


            //var paddedYExtent = util.domain.padYDomain(model.yExtent, 0.15);
            var paddedYExtent = model.yExtent


            //var tickValues = produceAnnotatedTickValues(yScale);
//console.warn('    function orders(selection)', model.yExtent)

            chartorder
                .xDomain(model.lim)
                .yDomain(paddedYExtent)
                //.yTickValues(tickValues)
                .yDecorate(function(s) {

                    s.selectAll('.tick')
                        .select('text')
                        .attr("transform", "translate(50,0)");


                    s.selectAll('.tick')
                        .select('path')
                        .attr("transform", "translate(90,0)")
                        //.append("path")
                        //.attr("d", 'M0,0L-6,0');


                    //s.selectAll('.tick')
                    //    .filter(function(d) { return d === latestPrice; })
                    //    .classed('closeLine', true)
                    //    .select('path')
                    //    //.attr('d', multi(calculateCloseAxisTagLine(yAxisWidth, 20)))
                    //    .attr("transform", "translate(55,0)");


                });

//if(model.data.med!='undefinded'){
    selection
        //.trackingLatest(model.trackingLatest)
        .datum(model.data)
        .call(chartorder);
//}


            //selection.datum(model.data.sell).call(sline);
            //selection.datum(model.data.buy).call(bline);


        //});

    }


    d3.rebind(orders, dispatch, 'on');

    orders.dimensionChanged = function (container) {
        container.style('height', $('#primary-container').height() + 'px');
    };

    return orders;
}
