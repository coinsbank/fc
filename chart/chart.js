import secondary from './secondary/secondary';
import nav from './nav';
import legend from './legend';
import xAxis from './xAxis';
import orders from './orders';
import primary from './primary';


export default {
    legend: legend,
    nav: nav,
    primary: primary,
    xAxis: xAxis,
    orders: orders,
    secondary: secondary

};
