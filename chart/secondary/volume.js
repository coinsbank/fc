import d3 from 'd3';
import fc from 'd3fc';
import util from '../../util/util';
import event from '../../event';
import base from './base';

export default function() {
    var dispatch = d3.dispatch(event.viewChange);
    var volumeBar = fc.series.bar()
      .yValue(function(d) { return d.volume; });


    var chart = base()
      .series([volumeBar])
      .yTicks(2)
      .on(event.viewChange, function(domain) {
          dispatch[event.viewChange](domain);
      });

    function volume(selection) {
        selection.each(function(model) {

            chart.xDomain(model.viewDomain);


            // Scale y axis
            var visibleData = util.domain.filterDataInDateRange(chart.xDomain(), model.data);
            var yExtent  = fc.util.extent()
                .fields('volume')(visibleData);

            //// Add percentage padding either side of extreme high/lows
            var paddedYExtent = util.domain.padYDomain(yExtent, 0.08);


            if (paddedYExtent[0] < 0) {
                paddedYExtent[0] = 0;
            }
            chart.yTickFormat(model.product.volumeFormat)
                .trackingLatest(model.trackingLatest)
                .xDomain(model.viewDomain)
                .yDomain(paddedYExtent)
                            .period(model.period)
                            .minimumPeriods(model.minimumPeriods);

            selection.datum(model.data)
                .call(chart);
        });
    }

    d3.rebind(volume, dispatch, 'on');

    volume.dimensionChanged = function(container) {
        chart.dimensionChanged(container);
    };

    return volume;
}
