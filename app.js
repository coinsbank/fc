/*global window */
import d3 from 'd3';
import fc from 'd3fc';
import chart from './chart/chart';
import model from './model/model';
import menu from './menu/menu';
import util from './util/util';
import event from './event';
import dataInterface from './data/dataInterface';
import coinbaseProducts from './data/coinbase/getProducts';
import coinbaseAdaptor from './data/adaptor/coinbase';
//import dataGeneratorAdaptor from './data/adaptor/dataGenerator';
import quandlAdaptor from './data/adaptor/quandl';
import webSocket from './data/coinbase/webSocket';
import formatProducts from './data/coinbase/formatProducts';
//
//export default {
//    chart: chart,
//    model: model,
//    menu: menu,
//    util: util,
//
//};


export default function() {

    var app = {};

    var appContainer = d3.select('#app-container');
    var chartsContainer = appContainer.select('#charts-container');
    var ordersContainer = appContainer.select('#chart-stock-container');
    var overlay = appContainer.select('#overlay');

    var minPeriods = 60;

    var containers = {
        app: appContainer,
        charts: chartsContainer,
        orders: ordersContainer,
        primary: chartsContainer.select('#primary-container'),
        secondaries: chartsContainer.selectAll('.secondary-container'),
        xAxis: chartsContainer.select('#x-axis-container'),
        navbar: chartsContainer.select('#navbar-container'),
        overlay: overlay,
        overlaySecondaries: overlay. selectAll('.overlay-secondary-container'),
        legend: appContainer.select('#legend'),
        suspendLayout: function(value) {
            var self = this;
            Object.keys(self).forEach(function(key) {
                if (typeof self[key] !== 'function') {
                    self[key].layoutSuspended(value);
                }
            });
        }
    };



    var locale = window.navigator.userLanguage || window.navigator.language;
    //console.log(locale)
    moment.locale(locale);

    var localeFormatter = d3.locale({
        "decimal": ",",
        "thousands": ".",
        "grouping": [3],
        "currency": ["€", ""],
        "dateTime": "%a %b %e %X %Y",
        "date": "%d-%m-%Y",
        "time": "%H:%M:%S",
        "periods": ["AM", "PM"],
        "days": moment.weekdays(),
        "shortDays": moment.weekdaysShort(),
        "months": moment.months(),
        "shortMonths": moment.monthsShort()
    });

    var tickFormat = localeFormatter.timeFormat.multi([
        ["%H:%M", function(d) { return d.getMinutes(); }],
        ["%H:%M", function(d) { return d.getHours(); }],
        ["%a %d", function(d) { return d.getDay() && d.getDate() != 1; }],
        ["%b %d", function(d) { return d.getDate() != 1; }],
        ["%B", function(d) { return d.getMonth(); }],
        ["%Y", function() { return true; }]
    ]);






    var hour6 = model.data.period({
        display: '6 Hr',
        seconds: 60 * 60 * 6,
        d3TimeInterval: {unit: d3.time.hour, value: 6},
        timeFormat: '%b %d %H'});

    var hour1 = model.data.period({
        display: '1 Hr',
        seconds: 60 * 60,
        d3TimeInterval: {unit: d3.time.hour, value: 1},
        timeFormat: '%b %d %H'});


    var minute30 = model.data.period({
        display: '30 Min',
        seconds: 60 * 30,
        d3TimeInterval: {unit: d3.time.minute, value: 30},
        timeFormat: '%b %d  %H:%M'});

    var minute10 = model.data.period({
        display: '10 Min',
        seconds: 60 * 10,
        d3TimeInterval: {unit: d3.time.minute, value: 10},
        timeFormat: '%b %d  %H:%M'});

    var minute5 = model.data.period({
        display: '5 Min',
        seconds: 60 * 5,
        d3TimeInterval: {unit: d3.time.minute, value: 5},
        timeFormat: '%b %d  %H:%M'});

    var minute1 = model.data.period({
        display: '1 Min',
        seconds: 60,
        d3TimeInterval: {unit: d3.time.minute, value: 1},
        timeFormat: '%b %d  %H:%M:%S'});


    var bitcoinSource = model.data.source(coinbaseAdaptor(), webSocket());


    if(config.defaultparams.pair=='btcusd'){
        var quandlSource = model.data.source(quandlAdaptor(), webSocket());
    }else {
        var quandlSource = model.data.source(quandlAdaptor());
    }

    var quandl = model.data.product({
        id: 'BTC-USD',
        display: 'GOOG',
        volumeFormat: 's',
        periods: [minute1, minute5, minute10, minute30, hour1, hour6],
        source: quandlSource
    });



    var primaryChartModel = model.chart.primary(quandl, minute1, minPeriods);
    var ordersChartModel = model.chart.orders(quandl);
    var secondaryChartModel = model.chart.secondary(quandl, minute1, minPeriods);
    var selectorsModel = model.menu.selectors();
    var xAxisModel = model.chart.xAxis(minute1);
    var navModel = model.chart.nav(minute1, minPeriods);
    var navResetModel = model.chart.navigationReset();
    var headMenuModel = model.menu.head([quandl], quandl, minute1);
    var legendModel = model.chart.legend(quandl, minute1);
    var overlayModel = model.menu.overlay();



    var charts = {
        primary: undefined,
        secondaries: [],
        xAxis: chart.xAxis(),
        orders: chart.orders(),
        navbar: undefined,
        legend: chart.legend()
    };

    var headMenu;
    var navReset;
    var selectors;


    function renderInternal() {
        if(primaryChartModel.data.length==0) return false;


        if (layoutRedrawnInNextRender) {
            containers.suspendLayout(false);
        }


        containers.primary.datum(primaryChartModel)
            .call(charts.primary);


        containers.orders.datum(ordersChartModel)
            .call(charts.orders);


        containers.legend.datum(legendModel)
            .call(charts.legend);


        containers.xAxis.datum(xAxisModel)
            .call(charts.xAxis);


        if(charts.navbar) {
            containers.navbar.datum(navModel)
                .call(charts.navbar);
            containers.app.select('#navbar-reset')
                .datum(navResetModel)
                .call(navReset);
        }

        containers.app.select('.head-menu')
            .datum(headMenuModel)
            .call(headMenu);

        containers.app.select('#selectors')
            .datum(selectorsModel)
            .call(selectors);

        containers.overlay.datum(overlayModel)
            .call(overlay);

        containers.secondaries.datum(secondaryChartModel)
            // TODO: Add component: group of secondary charts.
            // Then also move method layout.getSecondaryContainer into the group.
            .filter(function(d, i) { return i < charts.secondaries.length; })
            .each(function(d, i) {
                d3.select(this)
                    .attr('class', 'secondary-container ' + charts.secondaries[i].valueString)
                    .call(charts.secondaries[i].option);
            });






        if (layoutRedrawnInNextRender) {
            containers.suspendLayout(true);
            layoutRedrawnInNextRender = false;
        }
    }


    var render = fc.util.render(renderInternal);
    //app.render = fc.util.render(renderInternal);

    var layoutRedrawnInNextRender = false;

    function updateLayout() {
        layoutRedrawnInNextRender = true;
        util.layout(containers, charts);
    }

    function initialiseResize() {
        d3.select(window).on('resize', function() {
          //console.log()
            updateLayout();
            render();
        });
    }

    function onViewChange(domain) {
        var viewDomain = [domain[0], domain[1]];

        //console.log(domain[0], domain[1])
        //ordersChartModel.data
        //ordersChartModel.lim


        //definem lim by orders

        primaryChartModel.viewDomain = viewDomain;
        primaryChartModel.extentByOrders = ordersChartModel.dataLim;

        var visibleData = util.domain.filterDataInDateRange(viewDomain, primaryChartModel.data);


        var tmpMainExtent = fc.util.extent().fields(['low', 'high'])(visibleData);
        //var tmpMainExtent = primaryChartModel.extentByOrders;
        var tmpOrdersExtent = ordersChartModel.dataLim;

        //console.log('primaryChartModel.extentByOrders.length',primaryChartModel.extentByOrders.length);



//console.warn(tmpMainExtent[0], '>',tmpOrdersExtent[0])
//console.warn(tmpMainExtent[1], '<',tmpOrdersExtent[1])

            if (tmpMainExtent[0] < tmpOrdersExtent[0]){
                tmpOrdersExtent[0] = tmpMainExtent[0]
            }
            //
            if (tmpMainExtent[1] > tmpOrdersExtent[1]){
                tmpOrdersExtent[1] = tmpMainExtent[1]
            }

            ordersChartModel.yExtent = tmpOrdersExtent;
            primaryChartModel.extentByOrders=tmpOrdersExtent;

        //
        //}else{
        //    ordersChartModel.yExtent = fc.util.extent().fields(['low', 'high'])(visibleData);
        //}


        //console.log('primaryChartModel.viewDomain',ordersChartModel.viewDomain);
        //console.log('ordersChartModel.viewDomain',ordersChartModel.viewDomain);
        //
        //console.log('primaryChartModel.yExtent',ordersChartModel.yExtent);
        //console.log('ordersChartModel.yExtent',ordersChartModel.yExtent);


        //ordersChartModel.yExtent = ordersChartModel.dataLim;

        secondaryChartModel.viewDomain = viewDomain;
        xAxisModel.viewDomain = viewDomain;
        navModel.viewDomain = viewDomain;

        var trackingLatest = util.domain.trackingLatestData(
            primaryChartModel.viewDomain,
            primaryChartModel.data);

        primaryChartModel.trackingLatest = trackingLatest;
        ordersChartModel.trackingLatest = trackingLatest;
        secondaryChartModel.trackingLatest = trackingLatest;
        navModel.trackingLatest = trackingLatest;
        navResetModel.trackingLatest = trackingLatest;

        render();
    }

    function onPrimaryIndicatorChange(indicator) {
        console.log('indicator', indicator.valueString, indicator.isSelected)

        indicator.isSelected = !indicator.isSelected;
        //indOption.isSelected=app._settings.indicators[indOption.valueString];
        app._settings.indicators[indicator.valueString]=indicator.isSelected;
        app.settings.save(app._settings);

        updatePrimaryChartIndicators();
        render();
    }

    function onSecondaryChartChange(_chart) {
        console.log('_chart', _chart.option.name, _chart.isSelected);

        _chart.isSelected = !_chart.isSelected;

        app._settings.secondaries[_chart.option.name]=_chart.isSelected;
        app.settings.save(app._settings);
        updateSecondaryCharts();
        render();
    }

    function onCrosshairChange(dataPoint) {
        legendModel.data = dataPoint;
        render();
    }

    function resetToLatest() {
        var data = primaryChartModel.data;
        var dataDomain = fc.util.extent()
            .fields('date')(data);
        var navTimeDomain = util.domain.moveToLatest(dataDomain, data, 0.95);
        onViewChange(navTimeDomain);
    }

    function loading(isLoading) {
        appContainer.select('#loading-message')
            .classed('hidden', !isLoading);
        appContainer.select('#charts')
            .classed('hidden', isLoading);
    }

    function updateModelData(data) {
        primaryChartModel.data = data;
        secondaryChartModel.data = data;
        navModel.data = data;
        //render();
    }

    function updateModelSelectedProduct(product) {
        headMenuModel.selectedProduct = product;
        primaryChartModel.product = product;
        secondaryChartModel.product = product;
        legendModel.product = product;
    }

    function updateModelSelectedPeriod(period) {
        headMenuModel.selectedPeriod = period;
        xAxisModel.period = period;
        legendModel.period = period;
    }

    function initialisePrimaryChart() {
        return chart.primary()
            .on(event.crosshairChange, onCrosshairChange)
            .on(event.viewChange, onViewChange);
    }

    function initialiseNav() {
        return chart.nav()
            .on(event.viewChange, onViewChange);
    }

    function initialiseNavReset() {
        return menu.navigationReset()
            .on(event.resetToLatest, resetToLatest);
    }

    function initialiseDataInterface() {
        return dataInterface()
            .on(event.newTrade, function(data) {
                updateModelData(data);
                if(primaryChartModel.trackingLatest) {
                    var newDomain = util.domain.moveToLatest(
                        primaryChartModel.viewDomain,
                        primaryChartModel.data);
                    onViewChange(newDomain);
                }

            })
            .on(event.historicDataLoaded, function(data) {
                loading(false);
                updateModelData(data);
                legendModel.data = null;
                resetToLatest();
                updateLayout();
            })
            .on(event.historicFeedError, function(err) {
                console.log('Error getting historic data: ' + err); // TODO: something more useful for the user!
            })
            .on(event.streamingFeedError, function(err) {
                console.log('Error loading data from websocket: ' + err); // TODO: something more useful for the user!
            });
    }

    function initialiseHeadMenu(_dataInterface) {
        return menu.head()
            .on(event.dataProductChange, function(product) {
                loading(true);
                updateModelSelectedProduct(product.option);
                updateModelSelectedPeriod(product.option.periods[1]);
                console.log('.on(event.dataProductChange',product.option.periods[1])
                _dataInterface(product.option.periods[0].seconds, product.option);
                render();
            })
            .on(event.dataPeriodChange, function(period) {
              console.log('dataPeriodChange')

                loading(true);
                updateModelSelectedPeriod(period.option);
                console.log()
                _dataInterface(period.option.seconds);
              //console.log('dataPeriodChange',period)

                app._settings.period=period.selectedIndex;
                app.settings.save(app._settings);


                render();
            })
            .on(event.clearAllPrimaryChartIndicatorsAndSecondaryCharts, function() {
                primaryChartModel.indicators.forEach(deselectOption);
                charts.secondaries.forEach(deselectOption);

                updatePrimaryChartIndicators();
                updateSecondaryCharts();
                render();
            });
    }

    function selectOption(option, options) {
        console.log('selectOption',option.valueString, option.isSelected, options);

        //save settings
        app._settings.primaryChartSeries.selected=option.valueString;
        app.settings.save(app._settings);

        options.forEach(function(_option) {
            _option.isSelected = false;
        });
        option.isSelected = true;
    }

    function deselectOption(option) { option.isSelected = false; }

    //function fetchCoinbaseProducts() {
    //    coinbaseProducts(insertProductsIntoHeadMenuModel);
    //}

    //function insertProductsIntoHeadMenuModel(error, bitcoinProducts) {
    //    if (error) {
    //        console.log('Error getting Coinbase products: ' + error); // TODO: something more useful for the user!
    //    } else {
    //        var defaultPeriods = [minute1, minute5, hour1, day1];
    //        var productPeriodOverrides = d3.map();
    //        productPeriodOverrides.set('BTC-USD', [minute1, minute5, hour1, day1]);
    //        var formattedProducts = formatProducts(bitcoinProducts, bitcoinSource, defaultPeriods, productPeriodOverrides);
    //        headMenuModel.products = headMenuModel.products.concat(formattedProducts);
    //        render();
    //    }
    //}

    function initialiseSelectors() {
        return menu.selectors()
            .on(event.primaryChartSeriesChange, function(series) {

                primaryChartModel.series = series;
                selectOption(series, selectorsModel.seriesSelector.options);
                render();
            })
            .on(event.primaryChartIndicatorChange, onPrimaryIndicatorChange)
            .on(event.secondaryChartChange, onSecondaryChartChange);
    }

    function updatePrimaryChartIndicators() {
        primaryChartModel.indicators =
            selectorsModel.indicatorSelector.indicatorOptions.filter(function(option) {
                console.warn(option);
                return option.isSelected;
            });

        overlayModel.primaryIndicators = primaryChartModel.indicators;
    }

    function updateSecondaryCharts() {


        charts.secondaries =
            selectorsModel.indicatorSelector.secondaryChartOptions.filter(function(option) {
                return option.isSelected;
            });

        // TODO: This doesn't seem to be a concern of menu.
        charts.secondaries.forEach(function(chartOption) {
            chartOption.option.on(event.viewChange, onViewChange);
        });

        //console.warn(charts.secondaries);

        overlayModel.secondaryIndicators = charts.secondaries;
        // TODO: Remove .remove! (could a secondary chart group component manage this?).
        containers.secondaries.selectAll('*').remove();
        updateLayout();
    }

    function initialiseOverlay() {
        return menu.overlay()
            .on(event.primaryChartIndicatorChange, onPrimaryIndicatorChange)
            .on(event.secondaryChartChange, onSecondaryChartChange);
    }

    var ordersDataSet;


    app.updateOrderGraphData = function(data) {

        if(!data) return false;
        //console.log('ordersChartModel.generatd');
        //console.log(data);

        var res={
            buy:data.buy,
            sell:data.sell,
            med:data.med
        };



        //right side
        var amb=d3.max(res.buy, function(d) { return d.sumValue; });
        var ams=d3.max(res.sell, function(d) { return d.sumValue; });

        //left side
        var aminb=d3.min(res.buy, function(d) { return d.sumValue; });
        var amins=d3.min(res.sell, function(d) { return d.sumValue; });


        var rightlim = d3.min([amb,ams]);
        var leftlim = d3.max([aminb,amins]);

        //console.log('amount min max');
        //console.log(amb, ams);
        //console.log(aminb, aminb);
        //console.log(leftlim, rightlim);

        ordersChartModel.lim=[leftlim, rightlim];
        ordersChartModel.data=res;

        //var sumData = ordersChartModel.data.sell.concat(ordersChartModel.data.buy);

        var mb, ms;
        mb=d3.min(ordersChartModel.data.buy, function(d) { return d.rate; });
        ms=d3.max(ordersChartModel.data.sell, function(d) { return d.rate; });
        ordersChartModel.dataLim = [parseFloat(mb), parseFloat(ms)]//fc.util.extent().fields(['rate'])(sumData);
        //ordersChartModel.dataLim = [mb, ms]//fc.util.extent().fields(['rate'])(sumData);


//console.log(ordersChartModel.dataLim);



        if(primaryChartModel.data.length>0){
            onViewChange(primaryChartModel.viewDomain)
            //console.log(primaryChartModel.data.length)
            //console.log(primaryChartModel.viewDomain)

            //todo: check

            //var newDomain = util.domain.moveToLatest(
            //    primaryChartModel.viewDomain,
            //    primaryChartModel.data);
            //    onViewChange(newDomain);
           //onViewChange(primaryChartModel.viewDomain);


        }



    };

    app.setOrderGraphData = function(data) {


        //console.log('ordersChartModel.generatd');

        var res={
            buy:data.buy,
            sell:data.sell,
            med:data.med
        }


        //right side
        var amb=d3.max(res.buy, function(d) { return d.sumValue; });
        var ams=d3.max(res.sell, function(d) { return d.sumValue; });

        //left side
        var aminb=d3.min(res.buy, function(d) { return d.sumValue; });
        var amins=d3.min(res.sell, function(d) { return d.sumValue; });


        var rightlim = d3.min([amb,ams]);
        var leftlim = d3.max([aminb,amins]);

        ordersChartModel.lim=[leftlim, rightlim];
        ordersChartModel.data=res;

    };



    app.switchAnalysisTools = function(tool) {
        primaryChartModel.currentTool=tool;
        primaryChartModel.selectorsChanged=true;
        updateLayout();
        render();
    };



    app.initNavBar = function() {
        //console.log('initNavBar')
        charts.navbar = initialiseNav();
        navReset = initialiseNavReset();
        containers.navbar.datum(navModel).call(charts.navbar);
        updateLayout();
        render();
    };


    var settings = function() {

        var _merge = function(object1, object2) {
            // TODO: Merge (http://stackoverflow.com/questions/171251/how-can-i-merge-properties-of-two-javascript-objects-dynamically)

            var array = Array.isArray(object2);
            var dst = array && [] || {};

            if (array) {
                object1 = object1 || [];
                dst = dst.concat(object1);
                object2.forEach(function(e, i) {
                    if (typeof dst[i] === 'undefined') {
                        dst[i] = e;
                    } else if (typeof e === 'object') {
                        dst[i] = _merge(object1[i], e);
                    } else {
                        if (object1.indexOf(e) === -1) {
                            dst.push(e);
                        }
                    }
                });
            } else {
                if (object1 && typeof object1 === 'object') {
                    Object.keys(object1).forEach(function (key) {
                        dst[key] = object1[key];
                    });
                }
                Object.keys(object2).forEach(function (key) {
                    if (typeof object2[key] !== 'object' || !object2[key]) {
                        dst[key] = object2[key];
                    }
                    else {
                        if (!object1[key]) {
                            dst[key] = object2[key];
                        } else {
                            dst[key] = _merge(object1[key], object2[key]);
                        }
                    }
                });
            }

            return dst;
        };

        var _load = function(successCallback) {

            if (typeof successCallback === 'function') {

                app._settings = store.get('chartsettings');

                //settings fallback
                if(typeof app._settings === 'undefined'){
                    app._settings = config.defSettings;
                    store.set('chartsettings', config.defSettings);
                }

                setTimeout(function() {successCallback.call(null, app._settings);}, 0);
            }
        };

        var _save = function(settings, successCallback) {
            //_settings = _merge(_settings, settings);

           store.set('chartsettings', settings);

            if (typeof successCallback === 'function') {
                setTimeout(function() {successCallback.call(null);}, 0);
            }
        };

        var _clear = function(successCallback) {
            _settings = {};
            store.remove('chartsettings');

            if (typeof successCallback === 'function') {
                setTimeout(function() {successCallback.call(null);}, 0);
            }
        };

        return {
            load: _load,
            save: _save,
            clear: _clear
        };

    };


    app._settings={};

    app.switchNavBar = function() {

        if (!app.isNavDisplayInit) {
            app.initNavBar();
            app.isNavDisplayInit = true;
        }

        if (app._settings.navigator) {
            $('#navbar-row').hide();
            app._settings.navigator = false;
            app.settings.save(app._settings);

        } else {
            $('#navbar-row').show();
            app._settings.navigator = true;
            app.settings.save(app._settings);

        }
    };


    app.run = function() {
        //nav not init
        app.isNavDisplayInit = false;
        app.settings = settings();


        if (!store.enabled) {
            console.log('Local storage is not supported by your browser. Please disable "Private Mode", or upgrade to a modern browser.')
            return
        }

        var periodId=config.services[config.defaultparams.market].pairsAll[config.defaultparams.pair].interval; //2

        app.settings.load(function(){

            console.warn(app._settings);
            //periodId=app._settings.period;

            console.warn(app._settings.period);

                if(app._settings.period==false){
                    var selectedPeriod=quandl.periods[periodId];
                }else{
                    var selectedPeriod=quandl.periods[app._settings.period];
                }


            charts.primary = initialisePrimaryChart();

            var _dataInterface = initialiseDataInterface();
            headMenu = initialiseHeadMenu(_dataInterface);




            //restore settings for primary series
            selectorsModel.seriesSelector.options.forEach(function(seriesOption) {
                if(seriesOption.valueString===app._settings.primaryChartSeries.selected){
                    console.log(seriesOption.valueString);
                    primaryChartModel.series = seriesOption;
                    seriesOption.isSelected=true;
                }else{
                    seriesOption.isSelected=false;
                }
                //console.log('seriesOption:', seriesOption.valueString, seriesOption.isSelected, app._settings.primaryChartSeries.selected);
            });


            selectors = initialiseSelectors();
            overlay = initialiseOverlay();
            initialiseResize();
            updateLayout();

            //restore settings for indicators
            selectorsModel.indicatorSelector.indicatorOptions.forEach(function(indOption) {
                console.log('ind:', indOption.valueString, indOption.isSelected, app._settings.indicators[indOption.valueString]);
                indOption.isSelected=app._settings.indicators[indOption.valueString];
            });

            updatePrimaryChartIndicators();


            //restore settings for secondary chartsupdatePrimaryChartIndicator
            selectorsModel.indicatorSelector.secondaryChartOptions.forEach(function(chartOption) {
                //console.log('s:', chartOption.option.name, chartOption.isSelected);
                chartOption.isSelected=app._settings.secondaries[chartOption.option.name];
            });

            updateSecondaryCharts();


            if (app._settings.navigator) {
                $('#navbar-row').show();
                charts.navbar = initialiseNav();
                navReset = initialiseNavReset();
                app.isNavDisplayInit = true;
            }


            updateModelSelectedPeriod(selectedPeriod);//quandl.periods[period]
            _dataInterface(selectedPeriod.seconds, quandl);
       });




        //console.warn(config.services[config.defaultparams.market].pairsAll[config.defaultparams.pair].interval)


    };

    return app;
}