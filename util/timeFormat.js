import d3 from 'd3';
import fc from 'd3fc';


export default function() {

    var locale = window.navigator.userLanguage || window.navigator.language;
    //console.log(locale)

    moment.locale(locale);

    var localeFormatter = d3.locale({
        "decimal": ",",
        "thousands": ".",
        "grouping": [3],
        "currency": ["€", ""],
        "dateTime": "%a %b %e %X %Y",
        "date": "%d-%m-%Y",
        "time": "%H:%M:%S",
        "periods": ["AM", "PM"],
        "days": moment.weekdays(),
        "shortDays": moment.weekdaysShort(),
        "months": moment.months(),
        "shortMonths": moment.monthsShort()
    });

    var tickFormat = localeFormatter.timeFormat.multi([
        ["%H:%M", function(d) { return d.getMinutes(); }],
        ["%H:%M", function(d) { return d.getHours(); }],
        ["%a %d", function(d) { return d.getDay() && d.getDate() != 1; }],
        ["%b %d", function(d) { return d.getDate() != 1; }],
        ["%B", function(d) { return d.getMonth(); }],
        ["%Y", function() { return true; }]
    ]);


    return tickFormat;
}
