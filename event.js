export default {
    crosshairChange: 'crosshairChange',
    viewChange: 'viewChange',
    newTrade: 'newTrade',
    historicDataLoaded: 'historicDataLoaded',
    historicFeedError: 'historicFeedError',
    streamingFeedError: 'streamingFeedError',
    dataProductChange: 'dataProductChange',
    dataPeriodChange: 'dataPeriodChange',
    resetToLatest: 'resetToLatest',
    clearAllPrimaryChartIndicatorsAndSecondaryCharts: 'clearAllPrimaryChartIndicatorsAndSecondaryCharts',
    primaryChartSeriesChange: 'primaryChartSeriesChange',
    primaryChartYValueAccessorChange: 'primaryChartYValueAccessorChange',
    primaryChartIndicatorChange: 'primaryChartIndicatorChange',
    secondaryChartChange: 'secondaryChartChange',
    indicatorChange: 'indicatorChange'
};
